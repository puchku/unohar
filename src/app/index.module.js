'use strict';
angular
    .module('angularSeedApp', ['angular-loading-bar', 'ngIntlTelInput', 'permission', 'permission.ui', 'ngAnimate', 'slickCarousel', 'ngCookies', 'ngFileUpload', 'ngTouch',
        'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap', 'ngValidate', 'ngIntlTelInput', 'infinite-scroll','AngularPrint'
    ])

    .run(runBlock);
    function runBlock($rootScope, $location, $http, $state) {


    $rootScope.$on('$stateChangeStart', function(evt, to, params) {
        $rootScope.$broadcast('close menu');
        if (to.redirectTo) {
            evt.preventDefault();

            $state.go(to.redirectTo, params)
        }


    });
}
