(function() {
    'use strict';

    angular
        .module('angularSeedApp')
        .directive('acmeNavbar', acmeNavbar);

    /** @ngInject */
    function acmeNavbar() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html',
            scope: {
                creationDate: '='
            },
            controller: NavbarController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function NavbarController($scope, $state, $log,DataManagerService) {

            $scope.currentState = $state.current.name;
            $scope.screenWidth=screen.width
            var sessionObj;

            $scope.imageUrl= window.imageUrl;
            $scope.screenWidth = screen.width;



            $scope.logout = function() {
                var i = sessionStorage.length;
                while (i--) {
                    var key = sessionStorage.key(i);
                    sessionStorage.removeItem(key);
                }
                $scope.role = ''

                localStorage.clear();
                $state.go('landing');
            }



        }
    }

})();
