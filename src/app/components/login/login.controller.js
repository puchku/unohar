(function() {
    'use strict';

    angular
        .module('angularSeedApp')
        .controller('LoginController', ['$scope', '$state', 'DataManagerService', 'toastr', '$uiModal', LoginController]);

    /** @ngInject */
    function LoginController($scope, $state, DataManagerService, toastr, $uiModal) {
        $scope.personal_details = false;
        $scope.account_details = true;

        $scope.goToPersonal = function(form) {
            if (form.validate()) {
                $scope.personal_details = true;
                $scope.account_details = false;
            }
        }

        $scope.goToAccount = function() {
            $scope.personal_details = false;
            $scope.account_details = true;
        }

        $scope.signUpValidation = {
            rules: {
                userName: {
                    minlength: 6,
                    required: true
                },
                passwords:{
                  minlength:6,
                  required:true
                },
                re_password:{
                  required:true,
                  equalTo:"#pass"
                },
                fullName:{
                  minlength: 4 ,
                  required: true,
                  character: true
                },
                address:{
                  required: true
                },
                email:{
                  required:true,
                  email:true
                },
                phone:{
                  required: true,
                  minlength : 10,
                  maxlength: 10,
                  number: true
                }
            },
            messages: {
                userName: {
                    minlength: "userName must be of atleast 6 characters",
                    required: "UserName required"
                },
                passwords:{
                  minlength:"password must be of atleast 6 character",
                  required:"password cannot be empty"
                },
                re_password:{
                  required:"please enter the password again",
                  equalTo:"Two passwords are not same"
                },
                fullName:{
                  minlength: "Name must be of atleast 6 characters",
                  required: "Name required",
                  character : "Only characters are allowed"
                },
                address:{
                  required:"Address required"
                },
                email:{
                  required:"Email is required",
                  email:"Please enter a valid email"
                },
                phone:{
                  required: "Phone No. is required",
                  minlength: "Phone no. must be 10 digits.",
                  maxlength : "Phone no. must be 10 digits.",
                  number:"Phone no is not valid"
                }

            }
        }

        $scope.submit=function(form){
          if(form.validate()){
            console.log("sdnfusdf");
          }
        }

        $scope.openforgotPassModal = function() {
            var modalDefaults = {
                templateUrl: 'app/components/login/login/forgotPassModal.html',
                windowClass: 'custom_modal',
                size: 'md',
                scope: $scope

            };
            var modalOptions = {
                modalScope: {}
            };
            $uiModal.showModal(modalDefaults, modalOptions).then(function(result) {});

        };


    }

})();
