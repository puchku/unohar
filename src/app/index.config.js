(function() {
        'use strict';

        angular
            .module('angularSeedApp')
            .config(config);

        /** @ngInject */
        function config($logProvider, toastr, $validatorProvider, ngIntlTelInputProvider, cfpLoadingBarProvider, $httpProvider, $qProvider) {
            // Enable log
            $logProvider.debugEnabled(true);

            $qProvider.errorOnUnhandledRejections(false);


            // Set options third-party lib
            toastr.options.timeOut = 3000;
            toastr.options.positionClass = 'toast-top-right';
            toastr.options.preventDuplicates = true;
            toastr.options.progressBar = false;

            $validatorProvider.setDefaults({
                    errorElement: 'small',
                    errorClass: 'input-error'
            });
            $validatorProvider.addMethod("character", function(value, element) {
                    return this.optional(element) || /^([A-z_]+( [A-z_]+)*)$/.test(value);
            });
            $validatorProvider.addMethod("email", function(value, element) {
                    return this.optional(element) || /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/.test(value);
            });
            // $validatorProvider.addMethod("password", function(value, element) {
            //         return this.optional(element) || /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/.test(value);
            // });
            $validatorProvider.addMethod("alphaNumeric", function(value, element) {
                    return this.optional(element) || /^[a-z0-9]+$/i.test(value);
            });

                ngIntlTelInputProvider.set({
                    initialCountry: 'us'
                });

                cfpLoadingBarProvider.includeBar = true; cfpLoadingBarProvider.includeSpinner = true;


                $httpProvider.interceptors.push(function($q) {
                    return {
                        request: function(config) {
                            return config || $q.when(config);
                        },
                        requestError: function(request) {
                            return $q.reject(request);
                        },
                        response: function(response) {
                            return response || $q.when(response);
                        },
                        responseError: function(response) {
                            if (response && response.status === 404) {
                                //document.location.href = '#/404'
                            }
                            if (response.status === 500) {
                                //document.location.href = '#/500'
                            }
                            if (response.status === 401) {
                                //document.location.href = '#/sessionExpired'
                            }
                            // if (!navigator.onLine) {
                            //     //  document.location.href = '#/offline'
                            // }
                            return $q.reject(response);
                        }
                    };
                });


            }
        })();
