(function () {
    'use strict';
    angular.module('angularSeedApp')
        .service('$uiModal', ['$uibModal', $uiModal]);

    /** @ngInject */
    function $uiModal($uibModal) {
        var modalDefaults = {
            backdrop: true,
            keyboard: true,
            animation: true,
            bindToController: true,
            templateUrl: '',
            windowClass: 'scale',
            parent: angular.element(document.body)
        };

        var modalOptions = {
            closeButtonText: '',
            closeButtonClass: 'btn-default',
            actionButtonText: '',
            actionButtonClass: 'btn-primary',
            deleteButtonClass: '',
            headerText: '',
            bodyText: '',
            include: '',
            modalScope: {}
        };

        this.showModal = function (customModalDefaults, customModalOptions) {
            if (!customModalDefaults) customModalDefaults = new Object();
            customModalDefaults.backdrop = 'static';
            return this.show(customModalDefaults, customModalOptions);
        };

        this.show = function (customModalDefaults, customModalOptions) {
            var tempModalDefaults = {};
            var tempModalOptions = {};

            angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

            angular.extend(tempModalOptions, modalOptions, customModalOptions);

            return $uibModal.open({
                backdrop: customModalDefaults.backdrop,
                keyboard: customModalDefaults.keyboard,
                modalFade: customModalDefaults.modalFade,
                templateUrl: customModalDefaults.templateUrl,
                windowClass: customModalDefaults.windowClass,
                size: customModalDefaults.size,
                scope: customModalDefaults.scope,
                controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                    $scope.modalOptions = tempModalOptions;
                    $scope.modalOptions.ok = function (result) {
                        $uibModalInstance.close(result);
                    };
                    $scope.modalOptions.close = function (result) {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            }).result;
        };
    }

})();
