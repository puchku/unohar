var imageUrl = 'http://ilminnovates.com/jobcard/JobCardNew/';
(function() {
    'use strict';

    angular.module('angularSeedApp')
        .factory('DataManagerService', ['$http', 'Upload', '$q', DataManagerService]);

    /** @ngInject */
    function DataManagerService($http, Upload, $q) {

        return {
            getQuery: function(data) {
                var deferred = $q.defer();
                var promise = $http.get(serverUrl + data.controller + '/' + data.action + '.php', data).then(function(response) {
                    var time = response.config.responseTimestamp - response.config.requestTimestamp;
                    deferred.resolve(response.data);
                });
                return deferred.promise;
            },
            putQuery: function(data) {
                var deferred = $q.defer();

                var promise = $http.put(serverUrl + data.controller + '/' + data.action + '.php', data).then(function(response) {
                    deferred.resolve(response.data);
                });
                return deferred.promise;
            },
            postQuery: function(data) {
                var deferred = $q.defer();
                var promise = $http.post(serverUrl + data.controller + '/' + data.action + '.php', data.data).then(function(response) {
                    deferred.resolve(response.data);
                });
                return deferred.promise;
            },
            deleteQuery: function(data) {
                var deferred = $q.defer();
                var promise = $http.delete(serverUrl + data.controller + '/' + data.action + '.php', data).then(function(response) {
                    deferred.resolve(response.data);
                });
                return deferred.promise;
            },
            getJsonQuery: function(data) {
                var deferred = $q.defer();

                var promise = $http.get(data.controller + '/' + data.action, data).then(function(response) {
                    deferred.resolve(response.data);
                });
                return deferred.promise;
            },
            uploadPic: function(data) {
                var deferred = $q.defer();
                var promise = Upload.upload({
                    url: serverUrl + data.controller + "/" + data.action + ".php",
                    data: data.data
                }).then(function(response) {
                    deferred.resolve(response.data);
                });
                return deferred.promise;
            }
        };
    }

    angular.module('angularSeedApp')
        .factory('logTimeTaken', [logTimeTaken]);

    function logTimeTaken() {
        var logTimeTaken = {
            request: function(config) {
                config.requestTimestamp = new Date().getTime();
                return config;
            },
            response: function(response) {
                response.config.responseTimestamp = new Date().getTime();
                return response;
            }
        };
        return logTimeTaken;
    }


})();
