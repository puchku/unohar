(function () {
    'use strict';

    angular
        .module('angularSeedApp')
        .config(routeConfig);

    /** @ngInject */


    function routeConfig($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = false;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.interceptors.push('logTimeTaken');


        $stateProvider

            .state('login', {
                url: '/login',
                templateUrl: 'app/components/login/loginMaster.html',
                controller: 'LoginController',
                redirectTo: 'login.master'
            })
            .state('login.master', {
                url: '/master',
                templateUrl: 'app/components/login/login/login.html'
            })
            .state('login.register', {
                url: '/register',
                templateUrl: 'app/components/login/login/signup.html'
            })
            .state('login.mobileVerification', {
                url: '/mobileVerification',
                templateUrl: 'app/components/login/mobileVerification/mobileVerification.html'
            })
            .state('login.resetPassword', {
                url: '/resetPassword',
                templateUrl: 'app/components/login/resetPassword/resetPassword.html'
            })
            .state('login.resetPwdSuccess', {
                url: '/resetPwdSuccess',
                templateUrl: 'app/components/login/resetPwdSuccess/resetPwdSuccess.html'
            })
            .state('verification', {
                url: '/verification',
                templateUrl: 'app/components/verification/verification.html'
            })
            .state('shopkeeper', {
                url: '/shopkeeper',
                templateUrl: 'app/components/shopkeeper/master.html',
                controller: 'ShopkeeperController',
                redirectTo: 'shopkeeper.dashboard'
            })
            .state('shopkeeper.dashboard', {
                url: '/dashboard',
                templateUrl: 'app/components/shopkeeper/dashboard/dashboard.html'
            })


        $urlRouterProvider.otherwise('/login/master');

    }

})();
